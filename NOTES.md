# Notes

- https://github.com/joaopalmeiro/template-python-miniconda-notebook
- https://github.com/joaopalmeiro/dotfiles/blob/main/.condarc
- https://github.com/jupyterlab/extension-template/tree/v4.3.1?tab=readme-ov-file#use-the-template-to-create-extension: `conda install -c conda-forge "copier>=9.2,<10" jinja2-time`
- https://pypi.org/project/copier/
- https://pypi.org/project/jinja2-time/
- https://copier.readthedocs.io/en/stable/configuring/#data_file
- https://github.com/jupyterlab/extension-template/releases
- https://docs.conda.io/projects/conda/en/latest/user-guide/troubleshooting.html#id20:
  - "Occasionally, an installed package becomes corrupted. Conda works by unpacking the packages in the `pkgs` directory and then hard-linking them to the environment. Sometimes these get corrupted, breaking all environments that use them."
  - Solution: `conda install -f` or `conda clean --all --verbose`
- https://copier.readthedocs.io/en/stable/faq/#while-developing-why-the-template-doesnt-include-dirty-changes:
  - https://copier.readthedocs.io/en/stable/generating/#copying-dirty-changes
  - `-r, --vcs-ref VALUE:str` + "Git reference to checkout in `template_src`. If you do not specify it, it will try to checkout the latest git tag, as sorted using the PEP 440 algorithm. If you want to checkout always the latest version, use `--vcs-ref=HEAD`"
- https://formulae.brew.sh/cask/miniconda

## Commands

```bash
conda env remove --name try-jupyterlab-extension-template
```

```bash
conda deactivate && conda env remove --name try-jupyterlab-extension-template
```

```bash
conda deactivate && conda env remove --name try-jupyterlab-extension-template && conda env create -f environment.yml && conda activate try-jupyterlab-extension-template
```

```bash
rm -rf demo/
```

```bash
rm -rf local-demo/
```

```bash
rm -rf local-demo/ && copier copy --vcs-ref HEAD --data-file input.yml --trust ~/Documents/GitHub/extension-template local-demo
```

```bash
rm -rf demo/ local-demo/ && conda env remove --name try-jupyterlab-extension-template
```

```bash
which node && which npm
```

```bash
conda clean --all --verbose
```

```bash
pip uninstall jupyterlab && pip install jupyterlab --pre
```

```bash
pipx run --pip-args=jinja2-time==0.2.0 copier==9.3.1 copy --vcs-ref HEAD --trust ~/Documents/GitHub/extension-template local-demo
```

## Snippets

- [Re-exported symbol marked with `deprecated` doesn't get correct deprecation highlighting](https://github.com/microsoft/TypeScript/issues/53754) issue

```ts
export {
  /** @deprecated Deprecated. Use JavaScript template string instead. */
  _template as template,
};
```

instead of

```ts
/** @deprecated Deprecated. Use JavaScript template string instead. */
export { _template as template };
```
