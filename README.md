# try-jupyterlab-extension-template

Environment to test the [official JupyterLab extension template](https://github.com/jupyterlab/extension-template).

## Development

Install [Miniconda](https://conda.io/projects/conda/en/latest/index.html) (if necessary).

```bash
conda env create -f environment.yml
```

```bash
conda env list
```

```bash
conda activate try-jupyterlab-extension-template
```

```bash
conda list
```

```bash
copier copy --vcs-ref v4.3.7 --data-file input.yml --trust https://github.com/jupyterlab/extension-template demo
```

or

```bash
copier copy --vcs-ref HEAD --data-file input.yml --trust ~/Documents/GitHub/extension-template local-demo
```

```bash
cd demo/
```

or

```bash
cd local-demo/
```

```bash
conda deactivate
```
